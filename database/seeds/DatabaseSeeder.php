<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DocumentoTipoTableSeeder::class);
        $this->call(EstadoCivilTableSeeder::class);
        $this->call(FamiliaTipoTableSeeder::class);
        $this->call(PaisesTableSeeder::class);
        $this->call(DepartamentosTableSeeder::class);
        $this->call(MunicipiosTableSeeder::class);
        $this->call(MensajeTipoTableSeeder::class);
        $this->call(MensajeEstadoTableSeeder::class);
        $this->call(MensajeAccionTableSeeder::class);
        $this->call(EspecialidadTableSeeder::class);
    }
}
