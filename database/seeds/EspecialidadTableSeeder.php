<?php

use Illuminate\Database\Seeder;

class EspecialidadTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_especialidad')->insert([
            'descripcion' => 'Doctor'
        ]);
        DB::table('tbl_especialidad')->insert([
            'descripcion' => 'Especialista'
        ]);
    }
}
