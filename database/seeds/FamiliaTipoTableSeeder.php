<?php

use Illuminate\Database\Seeder;

class FamiliaTipoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_familia_tipo')->insert([
            'descripcion' => 'Madre'
        ]);
        DB::table('tbl_familia_tipo')->insert([
            'descripcion' => 'Padre'
        ]);
        DB::table('tbl_familia_tipo')->insert([
            'descripcion' => 'Hijo'
        ]);
        DB::table('tbl_familia_tipo')->insert([
            'descripcion' => 'Hija'
        ]);
        DB::table('tbl_familia_tipo')->insert([
            'descripcion' => 'Tio'
        ]);
        DB::table('tbl_familia_tipo')->insert([
            'descripcion' => 'Tia'
        ]);
        DB::table('tbl_familia_tipo')->insert([
            'descripcion' => 'Primo'
        ]);
        DB::table('tbl_familia_tipo')->insert([
            'descripcion' => 'Prima'
        ]);
        DB::table('tbl_familia_tipo')->insert([
            'descripcion' => 'Abuelo'
        ]);
        DB::table('tbl_familia_tipo')->insert([
            'descripcion' => 'Abuela'
        ]);
        DB::table('tbl_familia_tipo')->insert([
            'descripcion' => 'Otro'
        ]);
    }
}
