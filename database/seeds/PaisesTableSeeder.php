<?php

use Illuminate\Database\Seeder;

class PaisesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = 'app/SQL/paises.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Ciudades table seeded!');  
    }
}
