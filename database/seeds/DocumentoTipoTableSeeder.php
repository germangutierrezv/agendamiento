<?php

use Illuminate\Database\Seeder;

class DocumentoTipoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_documento_tipo')->insert([
            'descripcion' => 'Cedula de Ciudadanía',
            'acronimo' => 'CC'
        ]);
        DB::table('tbl_documento_tipo')->insert([
            'descripcion' => 'Registro Civil',
            'acronimo' => 'RC'
        ]);
        DB::table('tbl_documento_tipo')->insert([
            'descripcion' => 'Tarjeta de Identidad',
            'acronimo' => 'TI'
        ]);
        DB::table('tbl_documento_tipo')->insert([
            'descripcion' => 'Cedula de Extranjería',
            'acronimo' => 'CE'
        ]);
        DB::table('tbl_documento_tipo')->insert([
            'descripcion' => 'Pasaporte',
            'acronimo' => 'PS'
        ]);
        DB::table('tbl_documento_tipo')->insert([
            'descripcion' => 'Sin Información',
            'acronimo' => 'NN'
        ]);
        DB::table('tbl_documento_tipo')->insert([
            'descripcion' => 'NIT',
            'acronimo' => 'NIT'
        ]);
    }
}
