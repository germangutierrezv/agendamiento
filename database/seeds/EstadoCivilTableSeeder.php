<?php

use Illuminate\Database\Seeder;

class EstadoCivilTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_estado_civil')->insert([
            'estado' => 'Casado'
        ]);
        DB::table('tbl_estado_civil')->insert([
            'estado' => 'Divorciado'
        ]);
        DB::table('tbl_estado_civil')->insert([
            'estado' => 'Soltero'
        ]);
        DB::table('tbl_estado_civil')->insert([
            'estado' => 'Unión Libre'
        ]);
        DB::table('tbl_estado_civil')->insert([
            'estado' => 'Viudo'
        ]);
    }
}
