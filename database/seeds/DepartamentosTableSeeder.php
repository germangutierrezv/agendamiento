<?php

use Illuminate\Database\Seeder;

class DepartamentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = 'app/SQL/departamentos.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Departamentos table seeded!'); 
    }
}
