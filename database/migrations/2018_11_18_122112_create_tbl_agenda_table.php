<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblAgendaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_agenda', function (Blueprint $table) {
            $table->increments('id');
            $table->text('detalle');
            $table->datetime('inicio_cita');
            $table->datetime('fin_cita');
            $table->integer('duracion');
            $table->unsignedInteger('id_cliente');
            $table->unsignedInteger('id_especialista');
            $table->timestamps();
            $table->softDeletes();
            

            $table->foreign('id_cliente')
                ->references('id')
                ->on('tbl_cliente')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->foreign('id_especialista')
                ->references('id')
                ->on('tbl_especialista')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_agenda');
    }
}
