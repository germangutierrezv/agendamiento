<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClienteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_cliente', function (Blueprint $table) {
            $table->increments('id');
            $table->string('documento', 20);
            $table->string('nombre1', 50);
            $table->string('nombre2', 50)->nullable();
            $table->string('apellido1', 50);
            $table->string('apellido2', 50)->nullable();
            $table->enum('sexo',['F','M','O']);
            $table->text('direccion')->nullable();
            $table->date('fecha_nacimiento')->nullable();
            $table->string('contrato', 20)->nullable();
            $table->string('estrato', 20)->nullable();
            $table->string('historia', 20)->nullable();
            $table->string('folio', 20)->nullable();
            $table->string('carnet', 20)->nullable();
            $table->string('ficha_sisben', 20)->nullable();
            $table->string('correo', 100)->nullable();
            $table->string('telefono_1', 20)->nullable();
            $table->string('telefono_2', 20)->nullable();
            $table->string('celular_1', 20)->nullable();
            $table->string('celular_2', 20)->nullable();
            $table->string('foto', 20)->nullable();
            $table->unsignedInteger('id_documento');
            $table->unsignedInteger('id_regimen')->nullable();
            $table->unsignedInteger('id_tipoafiliacion')->nullable();
            $table->unsignedInteger('id_estadocivil');
            $table->unsignedInteger('id_convenio')->nullable();
            $table->unsignedInteger('id_pais');
            $table->unsignedInteger('id_departamento');
            $table->unsignedInteger('id_municipio');
            $table->timestamps();
            $table->softDeletes();

            $table->index('documento');

            $table->unique('correo');
            
            $table->foreign('id_documento')
                ->references('id')
                ->on('tbl_documento_tipo')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->foreign('id_regimen')
                ->references('id')
                ->on('tbl_regimen')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->foreign('id_tipoafiliacion')
                ->references('id')
                ->on('tbl_afiliacion_tipo')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->foreign('id_estadocivil')
                ->references('id')
                ->on('tbl_estado_civil')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->foreign('id_convenio')
                ->references('id')
                ->on('tbl_convenio')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->foreign('id_pais')
                ->references('id')
                ->on('tbl_paises')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->foreign('id_departamento')
                ->references('id')
                ->on('tbl_departamentos')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->foreign('id_municipio')
                ->references('id')
                ->on('tbl_municipios')
                ->onUpdate('cascade')
                ->onDelete('restrict');





        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cliente');
    }
}
