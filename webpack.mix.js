const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
   .scripts([
   		'./public/libs/angular/angular.min.js',
   		'./public/libs/lodash/dist/lodash.min.js',
   		'./public/libs/angular-ui-router/release/angular-ui-router.min.js',
   		'./public/libs/angular-animate/angular-animate.min.js',
   		'./public/libs/angular-bootstrap/ui-bootstrap.min.js',
   		'./public/libs/restangular/dist/restangular.min.js',
   		'./resources/assets/angular/app.js',
   		'./public/angular/app/app.js',
   		'./public/angular/controllers/controller.js'
	],'public/js/all.js');
