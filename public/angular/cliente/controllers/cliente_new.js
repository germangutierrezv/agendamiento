app.controller('AppClienteNewCtrl',['$scope','$state','Restangular','EstadosCivilFactory','DocumentosTipoFactory','PaisesFactory','DepartamentosFactory','MunicipiosFactory',
	'AfiliacionesFactory','ConveniosFactory','RegimenesFactory','swal','swalSrvc','$filter',
	function($scope,$state,Restangular,EstadosCivilFactory,DocumentosTipoFactory,PaisesFactory,DepartamentosFactory,MunicipiosFactory,
		AfiliacionesFactory,ConveniosFactory,RegimenesFactory,swal,swalSrvc,$filter) {

	var self = this

	EstadosCivilFactory.estados()
    	.then(function(response){
        	self.estados_civil = response.data.data;
    })

    DocumentosTipoFactory.documentos()
    	.then(function(response){
        	self.documentos_tipo = response.data.data;
    })

    PaisesFactory.paises()
    	.then(function(response){
        	self.paises = response.data.data;
    })

    AfiliacionesFactory.afiliaciones()
    	.then(function(response) {
    		self.afiliaciones = response.data.data;
   	})

    ConveniosFactory.convenios()
    	.then(function(response) {
    		self.convenios = response.data.data;
   	})

    RegimenesFactory.regimenes()
    	.then(function(response) {
    		self.regimenes = response.data.data;
   	})

    self.submit = function(form) {
        if(form == undefined) {
            swalSrvc.notify('Debes agregar los datos requeridos (*)','error')
            return
        }        
        if(!form.id_documento) {
            swalSrvc.notify('Debes seleccionar el tipo de documento','error')
            return
        }
        if(!form.documento) {
            swalSrvc.notify('Debes agregar el número de documento','error')
            return
        }
        if(!form.nombre1) {
            swalSrvc.notify('Debes agregar el primer nombre','error')
            return
        }
        if(!form.apellido1) {
            swalSrvc.notify('Debes agregar el primer apellido','error')
            return
        }
        if(!form.sexo) {
            swalSrvc.notify('Debes seleccionar el sexo','error')
            return
        }
        if(!form.id_estadocivil) {
            swalSrvc.notify('Debes seleccionar el estado civil','error')
            return
        }
        if(!form.correo) {
            swalSrvc.notify('Debes agregar el correo electrónico','error')
            return
        }
        if(!form.id_pais) {
            swalSrvc.notify('Debes seleccionar el pais','error')
            return
        }
        if(!form.id_departamento) {
            swalSrvc.notify('Debes seleccionar el departamento','error')
            return
        }
        if(!form.id_municipio) {
            swalSrvc.notify('Debes seleccionar el municipio','error')
            return
        }
        
        form.fecha_nacimiento = $filter('date')(form.fecha_nacimiento, "yyyy-MM-dd")


        let fd = new FormData();

    	for(let i in form) {
    		fd.append(i,form[i])
    	}

    	let input = document.querySelector('input[type=file]')                
      	let file = (input.files[0] ? input.files[0]: null)
      	if(file !== undefined || file !== NULL || file.type.match(/image.*/)) fd.append('imagen', file);

      	Restangular.one('cliente')
        .withHttpConfig({transformRequest: angular.identity})
        .customPOST(fd, undefined, undefined, {'Content-Type': undefined})
        .then(
            function (response) {
            	console.log(response)
            	swal({
					type: 'success',
					title: '',
				  	text: response.data.success,
				  	confirmButtonText: 'Aceptar',
  					confirmButtonColor: '#8CD4F5',
  					animation: true,
  					allowEscapeKey: true,
            	}).then((result) => {
				  if (result.value) {
            		$state.go('app.cliente', {}, {reload: true});
				  }
				})
            },
            function (error) {
            	console.log(error.data)
                if(error.status == 422) {
                    if(typeof(error.data) !== undefined && typeof(error.data) !== 'object') {
                        code = error.data;
                      }                     
                      if(typeof(error.data) === 'object'){
                          err = error.data.error;
                          code = '';
                          for(i in err){
                              code= code + err[i].toString() + "\n";
                          }
                      }
        
                      swalSrvc.notify(code,'error')
                      return                      
                }
            }
        )
    }

    self.changeCountry = function(id) {
        DepartamentosFactory.departamentos(id)
	        .then(function(response){
    	        self.departamentos = response.data.data;
        	});    

    }

    self.changeDepartament = function(id) {
        MunicipiosFactory.municipios(id)
	        .then(function(response){
    	        self.municipios = response.data.data;
        	});    

    }


}]);
