app.controller('AppClienteEditCtrl',['$scope','response','$state','Restangular','EstadosCivilFactory','DocumentosTipoFactory','PaisesFactory','DepartamentosFactory','MunicipiosFactory',
	'AfiliacionesFactory','ConveniosFactory','RegimenesFactory','swal','swalSrvc','$filter',
	function($scope,response,$state,Restangular,EstadosCivilFactory,DocumentosTipoFactory,PaisesFactory,DepartamentosFactory,MunicipiosFactory,
		AfiliacionesFactory,ConveniosFactory,RegimenesFactory,swal,swalSrvc,$filter) {

	var self = this

    self.user = response.data.data
    self.user.fecha_nacimiento = new Date(self.user.fecha_nacimiento)

	EstadosCivilFactory.estados()
    	.then(function(response){
        	self.estados_civil = response.data.data;
    })

    DocumentosTipoFactory.documentos()
    	.then(function(response){
        	self.documentos_tipo = response.data.data;
    })

    PaisesFactory.paises()
    	.then(function(response){
        	self.paises = response.data.data;
    })

    AfiliacionesFactory.afiliaciones()
    	.then(function(response) {
    		self.afiliaciones = response.data.data;
   	})

    ConveniosFactory.convenios()
    	.then(function(response) {
    		self.convenios = response.data.data;
   	})

    RegimenesFactory.regimenes()
    	.then(function(response) {
    		self.regimenes = response.data.data;
   	})

    DepartamentosFactory.departamentos(self.user.id_pais)
        .then(function(response){
            self.departamentos = response.data.data;
    })  

    MunicipiosFactory.municipios(self.user.id_departamento)
        .then(function(response){
            self.municipios = response.data.data;
    })      


    self.submit = function(form) {
        let id = form.id

    	form.fecha_nacimiento = $filter('date')(form.fecha_nacimiento, "yyyy-MM-dd HH:mm");
        let fd = new FormData()
        fd.append('id_documento',form.id_documento);
        fd.append('documento',form.documento);
        fd.append('nombre1',form.nombre1);
        fd.append('nombre2',form.nombre2);
        fd.append('apellido1',form.apellido1);
        fd.append('apellido2',form.apellido2);
        fd.append('correo',form.correo);
        fd.append('sexo',form.sexo);
        fd.append('id_estadocivil',form.id_estadocivil);
        fd.append('fecha_nacimiento',form.fecha_nacimiento);
        fd.append('telefono_1',form.telefono_1);
        fd.append('telefono_2',form.telefono_2);
        fd.append('celular_2',form.celular_2);
        fd.append('celular_1',form.celular_1);
        fd.append('id_pais',form.id_pais);
        fd.append('id_departamento',form.id_departamento);
        fd.append('id_municipio',form.id_municipio);
        fd.append('direccion',form.direccion);
        fd.append('contrato',form.contrato);
        fd.append('estrato',form.estrato);
        fd.append('historia',form.historia);
        fd.append('folio',form.folio);
        fd.append('carnet',form.carnet);
        fd.append('ficha_sisben',form.ficha_sisben);
        fd.append('id_convenio',form.id_convenio);
        fd.append('id_regimen',form.id_regimen);
        fd.append('id_tipoafiliacion',form.id_tipoafiliacion);

       /*  _.each(form, function (val, key) {
            fd.append(key, val);
        });*/ 

    	let input = document.querySelector('input[type=file]')                
      	let file = (input.files[0] ? input.files[0]: null)
      	if(file !== undefined || file !== NULL || file.type.match(/image.*/)) fd.append('imagen', file);

  	    Restangular.one('cliente/'+id)
        .withHttpConfig({transformRequest: angular.identity})
        .customPOST(
        fd, 
        undefined, 
        undefined, 
        {'Content-Type': undefined}
        )
        .then(
            function (response) {
            	console.log(response)
            	swal({
					type: 'success',
					title: '',
				  	text: response.data.success,
				  	confirmButtonText: 'Aceptar',
  					confirmButtonColor: '#8CD4F5',
  					animation: true,
  					allowEscapeKey: true,
            	}).then((result) => {
				  if (result.value) {
            		$state.go('app.cliente', {}, {reload: true});
				  }
				})
            },
            function (error) {
            	console.log(error.data)
                if(error.status == 422) {
                    if(typeof(error.data) !== undefined && typeof(error.data) !== 'object') {
                        code = error.data;
                      }                     
                      if(typeof(error.data) === 'object'){
                          err = error.data.error;
                          code = '';
                          for(i in err){
                              code= code + err[i].toString() + "\n";
                          }
                      }
        
                      swalSrvc.notify(code,'error')
                      return                                         
                }
                if(error.status != 422) {
            	    swalSrvc.notify(error.data.message,'error')
                    return  
                }
            }
        )
    }

    self.changeCountry = function(id) {
        DepartamentosFactory.departamentos(id)
	        .then(function(response){
    	        self.departamentos = response.data.data;
        	});    
    }

    self.changeDepartament = function(id) {
        MunicipiosFactory.municipios(id)
	        .then(function(response){
    	        self.municipios = response.data.data;
        	});    

    }


}]);
