app.controller('AppClienteCtrl',['$scope','response','$state','Restangular','DTOptionsBuilder','DTColumnBuilder','$compile','ClientesFactory',
	function($scope,response,$state,Restangular,DTOptionsBuilder, DTColumnBuilder,$compile,ClientesFactory) {

	let self = this;

	let dataSet = response.data.data

	self.dtOptions =  DTOptionsBuilder.newOptions()
     	.withOption('data', dataSet)
        .withOption('responsive', true)
        .withPaginationType('full_numbers')
        .withOption('createdRow', createdRow);


    self.dtColumns = [
        DTColumnBuilder.newColumn('id').withTitle('ID'),
        DTColumnBuilder.newColumn('documento').withTitle('Documento'),
        DTColumnBuilder.newColumn('nombre1').withTitle('Nombre'),
        DTColumnBuilder.newColumn('apellido1').withTitle('Apellido'),
        DTColumnBuilder.newColumn('correo').withTitle('Correo Electrónico'),
        DTColumnBuilder.newColumn(null).withTitle('Opciones').notSortable()
            .renderWith(actionsHtml)
    ];

    self.dtInstance={}

    self.displayTable = true
 
    function createdRow(row, data, dataIndex) {
      $compile(angular.element(row).contents())($scope)
    }

    function actionsHtml(data) {
    	return `
            <a class="btn btn-xs btn-raised btn-warning" id="btn-edit" title="Editar Cliente" ui-sref="app.cliente-edit({id: ${data.id}})">
                <i class="fa fa-edit"></i>
            </a>
            &nbsp;
            <button class="btn btn-xs btn-success btn-raised" id="btn" title="Agregar/Modificar Acompañante" ng-click="vm.acompanante(${data.id})">
                <i class="fa fa-users"></i>
            </button>
            <button class="btn btn-xs btn-danger btn-raised" id="btn-delete" title="Eliminar Cliente" ng-click="vm.delete(${data.id})">
                <i class="fa fa-trash-o"></i>
            </button>`	
    }
		
	self.delete = function(id) {
		swal({
	      title: '¿Desea Eliminar el cliente #'+id+'?',
	      text: 'No podra recuperar este registro!',
	      type: 'warning',
	      showCancelButton: true,
	      confirmButtonColor: '#DD6B55',
	      confirmButtonText: 'Si, Eliminar!',
	      html: false
	    }).then((result) => {
		  if (result.value) {
			Restangular.one('cliente/'+id).remove()
            	.then(function(response) {
            		console.log(response)
            		swal({
				    	title: '',
	      			 	text: response.data.success,
	      				type: 'success',
				    }).then((result) => {
				    	$state.reload()
				    })
                },
                function (error) {
                    swal({
				    	title: '',
	      			 	text: 'Ocurrio un error al aliminar el cliente',
	      				type: 'error',
				    })
                }
            );
		  }
		})

	}

    self.acompanante = function(id) {
        self.actbusqueda = false;
        self.datos = true;
        self.guardardatos = false;

        Restangular.one('cliente', id).get()
        .then( function (response) {
            self.cliente = response.data.data
        console.log(self.cliente)

        })

        ClientesFactory.clientes()
        .then(function(response){
                self.acomp = response.data.data
        })


        self.selectedAcomp=null

        $('#cliente-acom').modal("show")
    }

    self.actualizar= function(form) {
        console.log(form)
    }

    self.search= function() {
        self.actbusqueda = true
        self.guardardatos = false
    }
    self.add= function() {
        self.datos = true;
        self.actbusqueda = false
        self.guardardatos = true
    }

	}]);