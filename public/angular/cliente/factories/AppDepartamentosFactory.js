angular
.module('app')
.factory('DepartamentosFactory',['Restangular',
	function(Restangular) {		
		var dep = {
			departamentos : getDepartamentos 
		}

		function getDepartamentos(paisId){
			return Restangular.one('departamentos', paisId).get();
		}

        return dep;
}]);