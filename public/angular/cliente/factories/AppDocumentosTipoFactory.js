angular
.module('app')
.factory('DocumentosTipoFactory',['Restangular',
	function(Restangular) {

		var dTipo = {
			documentos : getDocumentosTipo 
		}

		function getDocumentosTipo(){
			return Restangular.all('tiposdocumento').customGET();
		}

        return dTipo;
}]);