angular
.module('app')
.factory('RegimenesFactory',['Restangular',
	function(Restangular) {		
		var reg = {
			regimenes : getRegimenes 
		}

		function getRegimenes(){
			return Restangular.all('regimenes').customGET();
		}

        return reg;
}]);