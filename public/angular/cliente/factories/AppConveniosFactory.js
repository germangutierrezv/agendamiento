angular
.module('app')
.factory('ConveniosFactory',['Restangular',
	function(Restangular) {		
		var conv = {
			convenios : getConvenios 
		}

		function getConvenios(){
			return Restangular.all('convenios').customGET();
		}

        return conv;
}]);