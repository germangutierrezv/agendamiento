angular
.module('app')
.factory('PaisesFactory',['Restangular',
	function(Restangular) {

		var paise = {
			paises : getPaises 
		}

		function getPaises(){
			return Restangular.all('paises').customGET();
		}

        return paise;
}]);