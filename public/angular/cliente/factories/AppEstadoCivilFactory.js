angular
.module('app')
.factory('EstadosCivilFactory',['Restangular',
	function(Restangular) {

		var eCivil = {
			estados : getEstadosCivil 
		}

		function getEstadosCivil(){
			return Restangular.all('estadoscivil').customGET();
		}

        return eCivil;
}]);