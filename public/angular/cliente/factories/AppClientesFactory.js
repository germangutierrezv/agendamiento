angular
.module('app')
.factory('ClientesFactory',['Restangular',
	function(Restangular) {		
		var cli = {
			clientes : getClientes 
		}

		function getClientes(){
			return Restangular.all('cliente').customGET();
		}

        return cli;
}]);