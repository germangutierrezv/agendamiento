angular
.module('app')
.factory('AfiliacionesFactory',['Restangular',
	function(Restangular) {		
		var afiliacion = {
			afiliaciones : getAfiliaciones 
		}

		function getAfiliaciones(){
			return Restangular.all('tiposafiliaciones').customGET();
		}

        return afiliacion;
}]);