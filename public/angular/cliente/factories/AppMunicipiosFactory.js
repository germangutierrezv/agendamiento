angular
.module('app')
.factory('MunicipiosFactory',['Restangular',
	function(Restangular) {		
		var mun = {
			municipios : getMunicipios 
		}

		function getMunicipios(departamentoId){
			return Restangular.one('municipios', departamentoId).get();
		}

        return mun;
}]);