var app = angular.module('app',[
	'restangular',
	'ui.router',
	'ngMask',
	ngSweetAlert2,
	'ui.grid', 
	'ui.grid.selection',
	'datatables',
	'ui.tinymce',
	'ui.calendar',
	'ui.bootstrap',
	'angularjs-datetime-picker',
	'ds.clock',
	'angucomplete'

]);

app.config(['$stateProvider','$urlRouterProvider', 'RestangularProvider','$locationProvider', 
	function($stateProvider, $urlRouterProvider, RestangularProvider, $locationProvider) {
		
		$locationProvider.hashPrefix('');
		//$locationProvider.html5Mode(true);
  
		console.log()
		RestangularProvider
		.setFullResponse(true)
		.setBaseUrl(base+'/api/')

	    $urlRouterProvider.otherwise('app/dashboard');

	    $stateProvider
	    
	    .state('app', {
	        url: '/app',
	        views: {
	            '': {
	                controller: 'AppCtrl as vm',
	                templateUrl: './angular/app/views/index.html'
	            }
	        }
	    })
	    //Dashboard
	    .state('app.dashboard', {
	        url: '/dashboard',
	        views: {
	            '': {
	                controller: 'AppDashboardCtrl as vm',
	                templateUrl: './angular/dashboard/views/index.html'
	            }
	        }
	    })
		//Cliente
	    .state('app.cliente', {
	        url: '/cliente',
	        views: {
	            '': {
	                controller: 'AppClienteCtrl as vm',
	                templateUrl: './angular/cliente/views/index.html'
	            }
	        },
	        resolve: {
	            response:  ['Restangular', function(Restangular){
	                return Restangular.all('cliente').doGET();
	            }]
        	}
	    })
	    //Cliente Nuevo
	    .state('app.cliente-add', {
	        url: '/cliente-add',
	        views: {
	            '': {
	                controller: 'AppClienteNewCtrl as vm',
	                templateUrl: './angular/cliente/views/new.html'
	            }
	        }
	    })
	    //Editar Cliente
	    .state('app.cliente-edit', {
	        url: '/cliente-edit/:id',
	        views: {
	            '': {
	                controller: 'AppClienteEditCtrl as vm',
	                templateUrl: './angular/cliente/views/edit.html',
	            }
	        },
	        resolve: {
	            response:  ['Restangular','$stateParams', function(Restangular, $stateParams){
	                return Restangular.all('cliente').doGET($stateParams.id,{});
	            }]
	        },
	    })
	    //Login
	    .state('login', {
	        url: '/login',
	        views: {
	            '': {
	                controller: 'AppLoginCtrl as vm',
	                templateUrl: './angular/login/views/index.html'
	            }
	        }
	    });   


}]);