app.service('swalSrvc',['swal',
	function(swal){
		var self = this;
		
		//Muestra las notificaciones
		self.notify=function(title,type,time){
			swal({
				toast: true,
				position: 'top-end',
            	showConfirmButton: false,
            	timer: time ? time: 4000,
            	type: type? type: 'success',
            	title: title
			})
		}

}]);