app.controller('AppLoginCtrl',['$scope','swalSrvc', 
	function($scope,swalSrvc) {
	
	let self = this

	self.ingreso = function(form) {
		if(form == undefined) {
	      swalSrvc.notify('Debe rellenar los campos de correo electrónico y contraseña','error')
	      return
	    } 

		if(!form.email) {
			swalSrvc.notify('El correo electrónico es requerido','error')
			return
		}

		if(!form.password) {
			swalSrvc.notify('La contraseña es requerida','error')
			return
		}

		console.log(form)


	}

	}]);