app.controller('AppAgendaCtrl',['$scope','ClientesFactory', 'Restangular','$state','swalSrvc',
	function($scope,ClientesFactory,Restangular,$state,swalSrvc) {
	
	let self = this	

  self.today = new Date()
  self.now = new Date() //moment().format('YYYY-MM-DDThh:mm') //new Date()
  console.log(self.now)
  self.duracion = 30

  ClientesFactory.clientes()
    .then(function(response){
      self.clientes = response.data.data
  })

  Restangular.all('especialista').customGET()
    .then(function(response) {
      self.especialistas = response.data.data
      _.each(self.especialistas, function(item) {
        item.descripcion = item.nombre1+' '+item.apellido1
      })
    })


  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();

	/* config object */
    self.uiConfig = {
      calendar:{
        height: 650,
        editable: true,
        displayEventTime: false,
        header:{
          left: 'today prev,next',
          center: 'title',
          right: 'month agendaWeek agendaDay'
        },

        dayClick: $scope.dayClick,
        eventDrop: $scope.alertOnDrop,
        eventResize: $scope.alertOnResize,
        eventClick: $scope.eventClick,
        viewRender: $scope.renderView
      }    
    };

    self.events = [
      {title: 'All Day Event',start: new Date(y, m, 1)},
      {title: 'Long Event',start: new Date(y, m, d - 5),end: new Date(y, m, d - 2)},
      {id: 999,title: 'Repeating Event',start: new Date(y, m, d - 3, 16, 0),allDay: false},
      {id: 999,title: 'Repeating Event',start: new Date(y, m, d + 4, 16, 0),allDay: false},
      {title: 'Birthday Party',start: new Date(y, m, d + 1, 19, 0),end: new Date(y, m, d + 1, 22, 30),allDay: false},
      {title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
    ];

    self.eventSource = {
            url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic",
            className: 'gcal-event',           // an option!
            currentTimezone: 'America/Chicago' // an option!
    };

//self.eventSources = [self.events, self.eventSource];
self.eventSources = [self.events];
    console.log(self.eventSource)

self.addEvent = function(form) {

console.log(form)
  if(!form.selectedCliente) {
    swalSrvc.notify('Debes seleccionar el Paciente','error')
    return
  }
  if(!form.detalle) {
    swalSrvc.notify('Debes agregar el detalle de la cita','error')
    return
  }
  
  var dateInit = new Date(form.inicio_cita)
  var minuteIni = dateInit.getMinutes();
  dateInit.setMinutes(minuteIni+form.duracion)

  var yearFin = dateInit.getFullYear()

  var monthFin = dateInit.getMonth()
  
  var dayFin = dateInit.getDate()
 
  var hourFin = dateInit.getHours()

  var minuteFin = dateInit.getMinutes() 

  var dateFin = new Date(yearFin,monthFin,dayFin,hourFin,minuteFin)

console.log(dateFin)
  self.events.push({
    'title': form.selectedCliente.originalObject.nombre1 + ' ' + form.selectedCliente.originalObject.apellido1+ '-' + form.detalle.substr(0,20),
    'start': form.inicio_cita,
    'end' : dateFin,
    'allDay': false 
  })

  //self.limpia()
}

self.limpia = function() {
  $state.go('app.agenda', {}, {reload: true});
}

	}]);
