<!DOCTYPE html>
<html ng-app="app" lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Agendamiento">
    <meta name="author" content="">

    <title>Agendamiento</title>

    <link href="{{ asset('css/AdminLTE/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/AdminLTE/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/AdminLTE/Ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/AdminLTE/AdminLTE.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/AdminLTE/bootstrap-material-design.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/AdminLTE/ripples.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/AdminLTE/MaterialAdminLTE.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/AdminLTE/skins/skin-blue.min.css') }}" rel="stylesheet">
      <!-- Google Font -->
    <link rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>

 
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div ui-view="">
  	
  	</div>

    <!-- jQuery -->
    <script src="{{ asset('js/AdminLTE/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('js/AdminLTE/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/AdminLTE/material.min.js') }}"></script>
    <script src="{{ asset('js/AdminLTE/ripples.min.js') }}"></script>
    <script>
        $.material.init();
    </script>
    <script src="{{ asset('js/AdminLTE/adminlte.min.js') }}"></script>

    <!--Angular-->
	<script src="{{ asset('libs/angular/angular.min.js')}} "></script>
	<script src="{{ asset('libs/lodash/dist/lodash.min.js') }}"></script>
	<script src="{{ asset('libs/angular-ui-router/release/angular-ui-router.min.js') }}"></script>        
	<script src="{{ asset('libs/angular-animate/angular-animate.min.js') }}"></script>        
	<script src="{{ asset('libs/angular-bootstrap/ui-bootstrap.min.js') }}"></script>        
    <script src="{{ asset('libs/restangular/dist/restangular.min.js') }}"></script>
	<script src="{{ asset('libs/angular-material/angular-material.min.js') }}"></script>
	<script src="{{ asset('angular/app.js') }}"></script>
	<script src="{{ asset('angular/app/controllers/app.js') }}"></script>
	<script src="{{ asset('angular/dashboard/controllers/dashboard.js') }}"></script>
	<script src="{{ asset('angular/cliente/controllers/cliente.js') }}"></script>
	<script src="{{ asset('angular/cliente/controllers/cliente_new.js') }}"></script>
	<!-- Custom Theme JavaScript -->
    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>


</body>

</html>
