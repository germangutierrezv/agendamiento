<!DOCTYPE html>
<html ng-app="app" lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Agendamiento">
    <meta name="author" content="">

    <title>Agendamiento</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{ asset('css/metisMenu.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-material-design.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('css/css/sb-admin-2.css') }}" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{ asset('css/morris.css') }}" rel="stylesheet">
    
    <!-- Custom Fonts -->
    <link href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <link href="{{ asset('css/bootstrap-toggle.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">

    <link href="{{ asset('libs/sweet-alert/sweetalert2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('libs/excel-angular/ui-grid.min.css') }}" rel="stylesheet">
    <link href="{{ asset('libs/angular-datatables/dist/css/angular-datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('libs/fullcalendar/fullcalendar.min.css') }}" rel="stylesheet">
    <link href="{{ asset('libs/angularjs-datetime-picker/angularjs-datetime-picker.css') }}" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div ui-view="">
  	
  	</div>

    <!-- jQuery -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('libs/angular-datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('libs/fullcalendar/lib/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('libs/fullcalendar/lib/moment.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-toggle.min.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ asset('js/metisMenu/metisMenu.min.js') }}"></script>

    <!-- Morris Charts JavaScript -->
    <script src="{{ asset('js/material.js') }}"></script>
    <script src="{{ asset('js/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('js/morrisjs/morris.min.js') }}"></script>

    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
    <script> var base="{{url('')}}";</script>

    <!--Angular-->
	<script src="{{ asset('libs/angular/angular.min.js')}} "></script>
    <script src="{{ asset('libs/angular-ui-tinymce/tinymce.js') }}"></script>
	<script src="{{ asset('libs/lodash/dist/lodash.min.js') }}"></script>
	<script src="{{ asset('libs/angular-ui-router/release/angular-ui-router.min.js') }}"></script>        
	<script src="{{ asset('libs/angular-animate/angular-animate.min.js') }}"></script>        
	<script src="{{ asset('libs/angular-bootstrap/ui-bootstrap.min.js') }}"></script>        
    <script src="{{ asset('libs/restangular/dist/restangular.min.js') }}"></script>
    <script src="{{ asset('libs/angular-material/angular-material.min.js') }}"></script>
    <script src="{{ asset('libs/angular-ngmask/ngMask.min.js') }}"></script>
    <!--Alerts-->
    <script src="{{ asset('libs/sweet-alert/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('libs/sweet-alert/ngSweetAlert2.min.js') }}"></script>
    <script type="text/javascript">
        var ngSweetAlert2 = window.ngSweetAlert2;
    </script>

    <script src="{{ asset('libs/angular-datatables/dist/angular-datatables.min.js') }}"></script>
    <script src="{{ asset('libs/angular-ui-tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('libs/fullcalendar/fullcalendar.js') }}"></script>
    <script src="{{ asset('libs/angular-uicalendar/calendar.js') }}"></script>
    <script src="{{ asset('libs/angularjs-datetime-picker/angularjs-datetime-picker.min.js') }}"></script>
    <script src="{{ asset('libs/angular-ui-clock/angular-clock.min.js') }}"></script>
    <script src="{{ asset('libs/angucomplete/angucomplete.js') }}"></script>


    <script src="{{ asset('libs/excel-angular/xlsx.full.min.js') }}"></script>
    <script src="{{ asset('libs/excel-angular/ods.js') }}"></script>
    <script src="{{ asset('libs/excel-angular/ui-grid.min.js') }}"></script>



	<script src="{{ asset('angular/app.js') }}"></script>
	<script src="{{ asset('angular/app/controllers/app.js') }}"></script>
	<script src="{{ asset('angular/dashboard/controllers/dashboard.js') }}"></script>
	<script src="{{ asset('angular/cliente/controllers/cliente.js') }}"></script>
    <script src="{{ asset('angular/cliente/controllers/cliente_new.js') }}"></script>
	<script src="{{ asset('angular/cliente/controllers/cliente_edit.js') }}"></script>
    <script src="{{ asset('angular/mensaje/controllers/mensaje.js') }}"></script>
    <script src="{{ asset('angular/mensaje/controllers/mensaje_new.js') }}"></script>
    <script src="{{ asset('angular/mensaje/controllers/mensaje_send.js') }}"></script>
    <script src="{{ asset('angular/agenda/controllers/agenda.js') }}"></script>
    <script src="{{ asset('angular/especialista/controllers/especialista.js') }}"></script>
    <script src="{{ asset('angular/especialista/controllers/especialista_new.js') }}"></script>
    <script src="{{ asset('angular/especialista/controllers/especialista_edit.js') }}"></script>
    <script src="{{ asset('angular/usuario/controllers/usuario.js') }}"></script>
    <script src="{{ asset('angular/login/controllers/login.js') }}"></script>


    <!-- Factories -->
    <script src="{{ asset('angular/cliente/factories/AppEstadoCivilFactory.js') }}"></script>
    <script src="{{ asset('angular/cliente/factories/AppDocumentosTipoFactory.js') }}"></script>
    <script src="{{ asset('angular/cliente/factories/AppPaisesFactory.js') }}"></script>
    <script src="{{ asset('angular/cliente/factories/AppDepartamentosFactory.js') }}"></script>
    <script src="{{ asset('angular/cliente/factories/AppMunicipiosFactory.js') }}"></script>
    <script src="{{ asset('angular/cliente/factories/AppAfiliacionesFactory.js') }}"></script>
    <script src="{{ asset('angular/cliente/factories/AppConveniosFactory.js') }}"></script>
    <script src="{{ asset('angular/cliente/factories/AppRegimenesFactory.js') }}"></script>
    <script src="{{ asset('angular/cliente/factories/AppClientesFactory.js') }}"></script>
    <script src="{{ asset('angular/mensaje/factories/AppMensajeFactory.js') }}"></script>
    <script src="{{ asset('angular/mensaje/factories/AppMensajeLimCarFactory.js') }}"></script>
    <script src="{{ asset('angular/especialista/factories/AppEspecialidadesFactory.js') }}"></script>

    <!-- Services -->
    <script src="{{ asset('angular/services/notification.srvc.js') }}"></script>

</body>

</html>
