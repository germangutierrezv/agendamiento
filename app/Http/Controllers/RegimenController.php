<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblRegimen;

class RegimenController extends Controller
{
    public function index()
    {
        $regimen = TblRegimen::select('id','descripcion')
                    ->orderBy('descripcion','ASC')
                        ->get();

        return response()->json(['success'=>true, 'data'=>$regimen],200);

    }
}
