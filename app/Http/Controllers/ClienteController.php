<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Models\TblCliente;
use App\Models\TblEstadoCivil;
use App\Models\TblDocumentoTipo;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = TblCliente::select()
                        ->get();

        return response()->json(['success'=>true, 'data'=>$clientes],200);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 422);
        }

        if($request->hasFile('imagen')) {

            $foto = $request->imagen;

            $extension = $foto->getClientOriginalExtension();

            $fullname = $request->documento.'.'.$extension;

            Storage::disk('cliente')->put($fullname, \File::get($foto)); 
        
            $request->merge(['foto' => $fullname]);
        }

        $cliente = TblCliente::create($request->all());

        if( !$cliente ) {
            return response()->json(['error'=>'Error al crear al cliente'], 422);
        }

        return response()->json(['success'=>'Cliente creado correctamente', 'cliente'=>$cliente],200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cliente = TblCliente::find($id);

        if( !$cliente ) {
            return response()->json(['error'=>'Cliente no encontrado'], 422);
        }

        return response()->json(['success'=>true, 'data'=>$cliente],200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cliente = TblCliente::find($id);

        if( !$cliente ) {
            return response()->json(['error'=>'Cliente no encontrado'], 422);
        }

        $validator = Validator::make($request->all(), [
            'documento' => 'required|max:20',
            'nombre1' => 'required|string|max:50',
            'nombre2' => 'max:50',
            'apellido1' => 'required|string|max:50',
            'apellido2' => 'max:50',
            'fecha_nacimiento'=> 'date',
            'correo'=> 'required|string|email|max:100|unique:tbl_cliente,correo,'.$request->id,
        ]);

        if($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 422);
        }

        if($request->hasFile('imagen')) {
            $foto = $request->imagen;

            $extension = $foto->getClientOriginalExtension();

            $fullname = $request->documento.'.'.$extension;

            if ( $cliente->foto != null )
                Storage::delete('clientes/'.$fullname);

            $request->merge(['foto' => $fullname]);

            Storage::disk('cliente')->put($fullname, \File::get($foto)); 
        
        }

        $input = $request->all();
        $cliente->update($input);
        //$cliente->fill($input)->save();

        return response()->json(['success'=>'Cliente actualizado exitosamente'], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cliente = TblCliente::find($id);

        if( !$cliente ) {
            return response()->json(['error'=>'Cliente no encontrado'], 422);
        }

        $cliente->delete();

        return response()->json(['success'=>'Cliente eliminado!'],200);

    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'documento' => 'required|max:20|unique:tbl_cliente,documento',
            'nombre1' => 'required|string|max:50',
            'nombre2' => 'max:50',
            'apellido1' => 'required|string|max:50',
            'apellido2' => 'max:50',
            'fecha_nacimiento'=> 'date',
            'correo'=> 'required|string|email|max:100|unique:tbl_cliente,correo',
        ]);
    }

    public function getEstadosCivil()
    {
        $estadoscivil = TblEstadoCivil::all();

        return response()->json(['success'=>true, 'data'=>$estadoscivil],200);
    }

    public function getDocumentoTipo()
    {
        $documentotipo = TblDocumentoTipo::all();

        return response()->json(['success'=>true, 'data'=>$documentotipo],200);
    }
}
