<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblAfiliacionTipo;

class AfiliacionTipoController extends Controller
{
    public function index()
    {
    	$tipos = TblAfiliacionTipo::select()
    				->orderBy('descripcion','ASC')
    					->get();

        return response()->json(['success'=>true, 'data'=>$tipos],200);

    }
}
