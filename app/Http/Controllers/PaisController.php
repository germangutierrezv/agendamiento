<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblPaises;
use App\Models\TblDepartamentos;
use App\Models\TblMunicipios;

class PaisController extends Controller
{
    public function getPaises()
    {
    	$paises = TblPaises::select()
    				->orderBy('nombre', 'ASC')
    					->get();

        return response()->json(['success'=>true, 'data'=>$paises],200);
    }

    public function getDepartamentos($id)
    {
    	$departamentos = TblDepartamentos::where('id_pais','=',$id)
    						->orderBy('nombre','ASC')
    							->get();

        return response()->json(['success'=>true, 'data'=>$departamentos],200);

    }

    public function getMunicipios($id)
    {
    	$municipios = TblMunicipios::where('id_departamento','=',$id)
    						->orderBy('nombre','ASC')
    							->get();
    
        return response()->json(['success'=>true, 'data'=>$municipios],200);
    
    }
}
