<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblConvenio;

class ConvenioController extends Controller
{
    public function index()
    {
        $convenios = TblConvenio::select('id','descripcion')
                        ->orderBy('descripcion','ASC')
                            ->get();

        return response()->json(['success'=>true, 'data'=>$convenios],200);
    }
}
