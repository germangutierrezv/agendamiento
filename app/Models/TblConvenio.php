<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TblConvenio extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table      = 'tbl_convenio';
	protected $primaryKey = 'id';
    protected $fillable   = ['descripcion','observacion'];

    protected $hidden  = [
        'created_at', 'updated_at', 'deleted_at'
    ];
}
