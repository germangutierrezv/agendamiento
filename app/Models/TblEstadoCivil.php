<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblEstadoCivil extends Model
{
    protected $table      = 'tbl_estado_civil';
	protected $primaryKey = 'id';
    protected $fillable   = ['estado'];

    protected $hidden  = [
        'created_at', 'updated_at'
    ];
}
