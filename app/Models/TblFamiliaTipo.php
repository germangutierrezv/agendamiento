<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblFamiliaTipo extends Model
{
    protected $table      = 'tbl_familia_tipo';
	protected $primaryKey = 'id';
    protected $fillable   = ['descripcion'];

    protected $hidden  = [
        'created_at', 'updated_at'
    ];
}
