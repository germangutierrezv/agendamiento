<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TblCliente extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table      = 'tbl_cliente';
	protected $primaryKey = 'id';
    protected $fillable   = ['documento','nombre1','nombre2','apellido1','apellido2','sexo','direccion',
    						'fecha_nacimiento','contrato','estrato','historia','folio','carnet','ficha_sisben',
    						'correo','telefono_1','telefono_2','celular_1','celular_2','foto','id_documento',
    						'id_regimen','id_tipoafiliacion','id_estadocivil','id_convenio','id_acompanante',
    						'id_pais','id_departamento','id_municipio'
    					];

    protected $hidden  = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function tipodocumento()
    {
        return $this->belongsTo('App\Models\TblDocumentoTipo','id_documento','id');
    }

    public function estadocivil()
    {
        return $this->belongsTo('App\Models\TblEstadoCivil','id_estadocivil','id');
    }

    public function paises() 
    {
        return $this->belongsTo('App\Models\TblPaises','id_pais','id');
    }

    public function departamentos()
    {
        return $this->belongsTo('App\Models\TblDepartamentos','id_departamento','id');
    }

    public function municipios()
    {
        return $this->belongsTo('App\Models\TblMunicipios','id_municipio','id');
    }

    public function convenios()
    {
        return $this->belongsTo('App\Models\TblConvenio','id_convenio','id');
    }

    public function regimenes()
    {
        return $this->belongsTo('App\Models\TblRegimen','id_regimen','id');
    }

    public function afiliaciones()
    {
        return $this->belongsTo('App\Models\TblAfiliacionTipo','id_tipoafiliacion','id');
    }


}
