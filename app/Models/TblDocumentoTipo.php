<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblDocumentoTipo extends Model
{
    protected $table      = 'tbl_documento_tipo';
	protected $primaryKey = 'id';
    protected $fillable   = ['descripcion'];

    protected $hidden  = [
        'created_at', 'updated_at'
    ];
}
