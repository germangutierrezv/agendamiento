<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TblRegimen extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table      = 'tbl_regimen';
	protected $primaryKey = 'id';
    protected $fillable   = ['descripcion'];

    protected $hidden  = [
        'created_at', 'updated_at', 'deleted_at'
    ];
}
