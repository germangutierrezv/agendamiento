<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblPaises extends Model
{
    protected $table      = 'tbl_paises';
	protected $primaryKey = 'id';
    protected $fillable   = ['nombre','iso'];

    protected $hidden  = [
        'created_at', 'updated_at'
    ];
}
