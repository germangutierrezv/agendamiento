<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblDepartamentos extends Model
{
    protected $table      = 'tbl_departamentos';
	protected $primaryKey = 'id';
    protected $fillable   = ['nombre','id_pais'];

    protected $hidden  = [
        'created_at', 'updated_at'
    ];

}
