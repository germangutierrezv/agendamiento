<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'API\UserController@login');

Route::resource('cliente','ClienteController',
	['except' => ['create','edit','update']
	]);
Route::post('cliente/{id}','ClienteController@update');

Route::resource('mensaje','MensajesController',
	['except' => ['create','edit']
	]);

Route::resource('especialista','EspecialistaController',
	['except' => ['create','edit']
	]);

Route::get('cancelarmensaje/{id}/{tipo}','MensajesController@cancel');
Route::get('mensaje-limcar/{id}','MensajesController@getLimiteCaracteres');

Route::get('estadoscivil','ClienteController@getEstadosCivil');
Route::get('tiposdocumento','ClienteController@getDocumentoTipo');
Route::get('paises','PaisController@getPaises');
Route::get('departamentos/{id}','PaisController@getDepartamentos');
Route::get('municipios/{id}','PaisController@getMunicipios');
Route::get('tiposafiliaciones','AfiliacionTipoController@index');
Route::get('convenios','ConvenioController@index');
Route::get('regimenes','RegimenController@index');
Route::get('panel','DashboardController@index');
Route::get('tiposmensajes','MensajesController@getTiposMensajes');
Route::get('mensajesaccion','MensajesController@getMensajesAccion');
Route::get('especialidades','EspecialistaController@getEspecialidades');